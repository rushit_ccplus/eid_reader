:: START
@echo off
echo EID Reader...
Title EID Reader (Please do not close this window) 
::  SET VARIABLES START
SET SCRIPT_NAME=EIDReader.exe
SET LOG_NAME=eidreader_log

SET SCRIPT_DRIVE=%CD:~0,1%
SET SCRIPT_PATH=%CD%
SET LOG_DIR=logs
::  SET VARIABLES END

::  CHECK SCRIPT ALREADY RUNNING OR NOT
TASKLIST /FI "IMAGENAME eq %SCRIPT_NAME:~0,25%*" 2>NUL | FIND /I /N "%SCRIPT_NAME:~0,25%">NUL

::  EXIT IF IT IS ALREADY RUNNING
IF "%ERRORLEVEL%"=="0" EXIT;

::  GO TO SPECIFIED SCRIPT_PATH
%SCRIPT_DRIVE%:
CD %SCRIPT_PATH%

::  CREATE LOG FOLDER IF NOT THERE
IF NOT EXIST %SCRIPT_PATH%\%LOG_DIR%\NUL MKDIR %SCRIPT_PATH%\%LOG_DIR%

::  CREATE CURRUNT TIMESTAMP FOR LOG FILE
@echo off
for /f "delims=" %%a in ('wmic OS Get localdatetime ^| find "."') do set DateTime=%%a
set Yr=%DateTime:~0,4%
set Mon=%DateTime:~4,2%
set Day=%DateTime:~6,2%
set Hr=%DateTime:~8,2%
set Min=%DateTime:~10,2%
set Sec=%DateTime:~12,2%
set BackupName=%Yr%%Mon%%Day%%Hr%%Min%%Sec%

::  START SCRIPT WITH LOG AND ERROR FILES
CMD /c %SCRIPT_NAME% >> "%LOG_DIR%/%LOG_NAME%.log%BackupName%" 2>> "%LOG_DIR%/%LOG_NAME%.err%BackupName%" &
::  END